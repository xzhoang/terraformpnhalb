data "aws_availability_zones" "available" {}

module "vpc" { #A
  source                           = "terraform-aws-modules/vpc/aws"
  version                          = "2.64.0"
  name                             = "${var.namespace}-vpc"
  cidr                             = "10.0.0.0/16"
  azs                              = data.aws_availability_zones.available.names
  private_subnets                  = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets                   = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  database_subnets                 = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
  
  create_database_subnet_group     = true
  enable_nat_gateway               = true
  single_nat_gateway               = true
}

module "lb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  name        = "alb_sg"
  description = "Security group open ports 22,80,443 "
  vpc_id      = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp","http-80-tcp","ssh-tcp"]

}


module "websvr_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  name        = "web-srv"
  description = "Security group open ports 22,80,443 "
  vpc_id      = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp","http-80-tcp","ssh-tcp"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 3000
      to_port     = 3000
      protocol    = "tcp"
      description = "nodejs ports"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

}

module "db_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  name        = "db-srv"
  description = "Security group open ports 3306 "
  vpc_id      = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      description = "mysql ports"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

}

