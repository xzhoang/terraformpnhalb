resource "aws_iam_policy" "iam_policy_logs_rds" {
  name        = "EC2 access Cloudwatch, RDS"
  description = "Enable EC2 access RDS, Cloudwatch"
  path        = "/"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
			"rds:*",
			"logs:*",
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}



resource "aws_iam_role" "iam_role_to_ec2" {
  name        = "EC2 role access Logs rds"
  description = "Attact this role to EC2 "

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "role_to_ec2" {
  role       = aws_iam_role.iam_role_to_ec2.name
  policy_arn = aws_iam_policy.iam_policy_logs_rds.arn
}
